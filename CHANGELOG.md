# CHANGELOG

## 4.0.2 - 2023-05-02

### Changed

- Compatible SPIP 4.2


## 4.0.1 - 2022-11-27

### Fixed

- Le bouton "lier des deux côtés" ne marchait plus en cas de recherche par arborescence
