<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-a2a?lang_cible=ar
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a2a_description' => 'يتيح هذا الملحق ربط مقالات ببعضها بصفة ظرفية دون الحاجة لاستخدام المفاتيح. يمكن استخدامه لوضع روابط من نوع «المزيد».

ويسمح هذا الاصدار الجديد ايضاً إضفاء أنواع على العلاقات بين المقالات. وظيفة الإضفاء هذه ليست نشطة افتراضياً للحفاظ على أداء الملحق التقليدي (راجعوا التوثيق). ',
	'a2a_slogan' => 'من أجل ربط المقالات ببعضها'
);
