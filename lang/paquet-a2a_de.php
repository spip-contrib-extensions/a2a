<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-a2a?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a2a_description' => 'Dieses Plugin verlinkt Artikel ohne Schlagworte untereinander. Es eignet sich um Links zu « Artikel zum gleichen Thema » anzuzeigen.
	
	Diese neue Version ermöglich den Links Typen zuzuordnen. Diese Typisierung ist in der Grundeinstellung nicht aktiv, um die Kompatibilität mit älteren Versionen zu erhalten (siehe Dokumentation).',
	'a2a_slogan' => 'Artikel untereinander verlinken'
);
